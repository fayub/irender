package irender

import (
	"html/template"
	"net/http"
)

// HTML takes data as interface and renders given templates
func HTML(w http.ResponseWriter, data interface{}, templates ...string) error {
	tmpl := template.Must(template.ParseFiles(templates...))
	return tmpl.Execute(w, data)
}
